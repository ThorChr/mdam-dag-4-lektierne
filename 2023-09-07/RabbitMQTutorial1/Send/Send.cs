﻿using System.Text;
using RabbitMQ.Client;

// Create a connection to the server.
ConnectionFactory factory = new() { HostName = "localhost" };
using var connection = factory.CreateConnection();
using var channel = connection.CreateModel();

// Declare a queue for us to send to; then we can publish a message to the queue.
channel.QueueDeclare(
    queue: "test",
    durable: false,
    exclusive: false,
    autoDelete: false,
    arguments: null
);

Console.WriteLine("Entering message loop. Write exit to leave.\n");

while(true)
{
    Console.Write("> ");
    string userInput = Console.ReadLine() ?? String.Empty;

    if(userInput.Equals("exit", StringComparison.OrdinalIgnoreCase))
    {
        break;
    }

    // Seems like we create a body, by converting the message we want to send into bytes. And these bytes are then out body.
    string message = userInput;
    var body = Encoding.UTF8.GetBytes(message);

    // Guess we are publishing our message here, to our previously created queue.
    channel.BasicPublish(
        exchange: String.Empty,
        routingKey: "test",
        basicProperties: null,
        body: body
    );

    Console.WriteLine($" [x] Sent {message}");
}

Console.WriteLine(" Press [enter] to exit.");
Console.ReadLine();