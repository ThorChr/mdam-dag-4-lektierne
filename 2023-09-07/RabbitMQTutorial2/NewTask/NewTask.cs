﻿using System.Text;
using RabbitMQ.Client;

// Create a connection to the server.
ConnectionFactory factory = new() { HostName = "localhost" };
using var connection = factory.CreateConnection();
using var channel = connection.CreateModel();

// Declare a queue for us to send to; then we can publish a message to the queue.
channel.QueueDeclare(
    queue: "test",
    durable: false,
    exclusive: false,
    autoDelete: false,
    arguments: null
);

// Seems like we create a body, by converting the message we want to send into bytes. And these bytes are then out body.
var message = GetMessage(args);
var body = Encoding.UTF8.GetBytes(message);

// Guess we are publishing our message here, to our previously created queue.
channel.BasicPublish(
    exchange: String.Empty,
    routingKey: "test",
    basicProperties: null,
    body: body
);

Console.WriteLine($" [x] Sent {message}");

Console.WriteLine(" Press [enter] to exit.");
Console.ReadLine();

static string GetMessage(string[] args)
{
    // Basically, lav en besked ved at yoink whatever arguments der er og så kombiner dem sammen med et space. Hvis der ingen er, smid helloworld.
    return ((args.Length > 0) ? String.Join(" ", args) : "Hello World!");
}