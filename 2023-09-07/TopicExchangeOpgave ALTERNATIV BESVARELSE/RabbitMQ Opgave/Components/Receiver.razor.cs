﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ_Opgave.Models;
using Microsoft.AspNetCore.Components;
using System.Text.Json;
using System.Threading.Channels;

namespace RabbitMQ_Opgave.Components;

public partial class Receiver
{
    public Receiver()
    {
        _messageList = new();
    }

    [Parameter]
    public string RoutingKey { get; set; }

    private List<SenderModel> _messageList;

    private string _exchange = "topic_logs";
    private string _queueName;
    private EventingBasicConsumer _consumer;
    private IModel _channel;


    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender)
        {
            // Create channel.
            var factory = new ConnectionFactory { HostName = "localhost" };
            var connection = factory.CreateConnection();
            _channel = connection.CreateModel();
            _channel.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Topic);

            // Set fields.
            _queueName = _channel.QueueDeclare().QueueName;

            // Create consumer.
            _consumer = new EventingBasicConsumer(_channel);

            // Do the event.
            _consumer.Received += ReceiveInfo;

            // Bind to queue.
            _channel.QueueBind(queue: _queueName, exchange: _exchange, routingKey: RoutingKey);

            // Start consuming.
            _channel.BasicConsume(_queueName, true, _consumer);
        }

        base.OnAfterRender(firstRender);
    }


    public void ReceiveInfo(object? model, BasicDeliverEventArgs ea)
    {
        var body = ea.Body.ToArray();
        var message = Encoding.UTF8.GetString(body);
        var deserializedMessage = JsonSerializer.Deserialize<SenderModel>(message);

        InvokeAsync(() =>
        {
            _messageList.Add(deserializedMessage);
            StateHasChanged();
        });
    }
}
