﻿namespace RabbitMQ_Opgave.Models
{
    public class SenderModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public bool Booked { get; set; }
    }
}
