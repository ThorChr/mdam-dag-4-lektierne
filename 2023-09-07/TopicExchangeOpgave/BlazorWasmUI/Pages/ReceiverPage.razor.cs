﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMqLibrary.Services;

namespace MauiBlazorApp.Pages;

public partial class ReceiverPage
{
    private FunMessageYesYesYeeeees _messageToYeet;
    private IMessageReceiver _messageReceiver;
    private string _exchange = "MikoMiku";
    private string _queueName;
    private EventingBasicConsumer _consumer;

    public ReceiverPage()
    {
        _messageToYeet = new();
        var channel = RabbitMqConnectionFactory.CreateChannel("localhost", _exchange, ExchangeType.Topic);
        _messageReceiver = new RabbitMqService(channel);
        _consumer = new EventingBasicConsumer(channel);
        _consumer.Received += HandleMessageReceived;
    }

    public void BindToQueue(string bindingKey) => _messageReceiver.BindQueue(_queueName, bindingKey, _exchange);

    public void HandleMessageReceived(object? model, BasicDeliverEventArgs args)
    {
        Console.WriteLine("Message received.");
    }
}
