﻿using Microsoft.AspNetCore.Components;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMqLibrary.Services;
using System.Text;
using System.Text.Json;

namespace MauiBlazorApp.Pages;

public partial class ReceiverPage
{
    [Parameter]
    public string BindingKey { get; set; }

    private IMessageReceiver _messageReceiver;
    private string _exchange = "MikoMiku";
    private string _queueName;
    private EventingBasicConsumer _consumer;

    private List<string> _messagesReceived = new List<string>();

    protected override void OnInitialized()
    {
        var channel = RabbitMqConnectionFactory.CreateChannel("localhost", _exchange, ExchangeType.Topic);
        _messageReceiver = new RabbitMqService(channel);
        _queueName = _messageReceiver.GetQueueName();
        _consumer = new EventingBasicConsumer(channel);
        _consumer.Received += HandleMessageReceived;

        // Bind to our queues.
        _messageReceiver.BindQueue(_queueName, _exchange, BindingKey);

        // Start consuming.
        channel.BasicConsume(_queueName, true, _consumer);

        base.OnInitialized();
    }

    public void HandleMessageReceived(object? model, BasicDeliverEventArgs args)
    {
        string jsonPayload = Encoding.UTF8.GetString(args.Body.ToArray());
        var payloadAsObject = JsonSerializer.Deserialize<FunMessageYesYesYeeeees>(jsonPayload);

        string message = $"User '{payloadAsObject.Name}' with email '{payloadAsObject.Email}' has done something. Booking status: {payloadAsObject.Booked}";
        
        // Switch to the UI thread to update UI components
        InvokeAsync(() =>
        {
            _messagesReceived.Add(message);
            StateHasChanged();
        });
    }
}
