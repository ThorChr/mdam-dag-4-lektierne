﻿using RabbitMqLibrary.Models;
using RabbitMQ.Client;

namespace RabbitMqLibrary.Factories;

public class RabbitMqConnectionFactory
{
    public static IModel CreateChannel(string hostName, string exchange, string exchangeType)
    {
        var factory = new ConnectionFactory { HostName = hostName };
        var connection = factory.CreateConnection();
        var channel = connection.CreateModel();
        channel.ExchangeDeclare(exchange: exchange, type: exchangeType);

        return channel;
    }

    public static IModel CreateChannel(RabbitMqFactoryChannelArguments arguments) => CreateChannel(arguments.Hostname, arguments.Exchange, arguments.ExchangeType);
}
