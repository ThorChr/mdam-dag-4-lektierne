﻿using RabbitMqLibrary.Models;

namespace RabbitMqLibrary.Interfaces;

public interface IMessageReceiver
{
    void BindQueue(string queueName, string exchange, string routingKey);
    void BindQueue(QueueModel queueModel);
    string GetQueueName();
}
