﻿using RabbitMqLibrary.Models;

namespace RabbitMqLibrary.Interfaces;

public interface IMessageSender
{
    void SendMessage(string routingKey, string message, string exchange);
    void SendMessage(MessageModel messageModel);
}
