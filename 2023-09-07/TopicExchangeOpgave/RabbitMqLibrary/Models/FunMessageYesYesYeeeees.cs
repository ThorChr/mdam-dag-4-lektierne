﻿namespace RabbitMqLibrary.Models;

public class FunMessageYesYesYeeeees
{
    public string Name { get; set; }
    public string Email { get; set; }
    public bool Booked { get; set; }

    public FunMessageYesYesYeeeees(string name, string email, bool booked)
    {
        Name = name;
        Email = email;
        Booked = booked;
    }

    public FunMessageYesYesYeeeees() 
    {
        Name = String.Empty;
        Email = String.Empty;
        Booked = false;
    }
}