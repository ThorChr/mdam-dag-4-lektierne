﻿namespace RabbitMqLibrary.Models;

public class MessageModel
{
    public string RoutingKey { get; set; } = String.Empty;
    public string Message { get; set; } = String.Empty;
    public string Exchange { get; set; } = String.Empty;
}
