﻿namespace RabbitMqLibrary.Models;

public class RabbitMqFactoryChannelArguments
{
    public string Hostname { get; set; } = String.Empty;
    public string Exchange { get; set; } = String.Empty;

    /// <summary>
    /// Seems like this can actually be a string. Don't have to be of type ExchangeType, so... Nice.
    /// </summary>
    public string ExchangeType { get; set; } = String.Empty;
}
