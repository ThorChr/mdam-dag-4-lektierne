﻿using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMQ.Client;
using System.Text;

namespace RabbitMqLibrary.Services;

public class RabbitMqService : IMessageSender, IMessageReceiver
{
    private readonly IModel _channel;

    public RabbitMqService(IModel channel)
    {
        _channel = channel;
    }

    public void SendMessage(string routingKey, string message, string exchange)
    {
        var body = Encoding.UTF8.GetBytes(message);
        _channel.BasicPublish(exchange: exchange, routingKey: routingKey, basicProperties: null, body: body);
    }

    public void SendMessage(MessageModel messageModel) 
        => SendMessage(messageModel.RoutingKey, messageModel.Message, messageModel.Exchange);

    public void BindQueue(string queueName, string exchange, string routingKey)
    {
        _channel.QueueBind(queue: queueName, exchange: exchange, routingKey: routingKey);
    }

    public void BindQueue(QueueModel queueModel) 
        => BindQueue(queueModel.QueueName, queueModel.Exchange, queueModel.RoutingKey);

    public string GetQueueName()
    {
        return _channel.QueueDeclare().QueueName;
    }
}
