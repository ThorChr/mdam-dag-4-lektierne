﻿using Microsoft.AspNetCore.Components;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using RabbitMQ_Opgave.Models;
using System.Text.Json;
using System.Text;

namespace RabbitMQ_Opgave.Components
{
    public partial class DeadLetterExchangeReceiver
    {
        public DeadLetterExchangeReceiver()
        {
            _messageList = new();
        }

        private List<SenderModel> _messageList;
        private string _exchange = "dlx_AdminAppTask";
        private string _queueName;
        private EventingBasicConsumer _consumer;
        private IModel _channel;


        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                // Create channel.
                var factory = new ConnectionFactory { HostName = "localhost" };
                var connection = factory.CreateConnection();
                _channel = connection.CreateModel();
                _channel.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Fanout);
                _queueName = $"{_exchange}_queue";
                _channel.QueueDeclare(_queueName, true, false, false, null);

                // Create consumer.
                _consumer = new EventingBasicConsumer(_channel);

                // Do the event.
                _consumer.Received += ReceiveInfo;

                // Bind to queue.
                _channel.QueueBind(_queueName, _exchange, "");

                // Start consuming.
                _channel.BasicConsume(_queueName, true, _consumer);
            }

            base.OnAfterRender(firstRender);
        }


        public void ReceiveInfo(object? model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var deserializedMessage = JsonSerializer.Deserialize<SenderModel>(message);

            InvokeAsync(() =>
            {
                _messageList.Add(deserializedMessage);
                StateHasChanged();
            });
        }
    }
}
