﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ_Opgave.Models;
using Microsoft.AspNetCore.Components;
using System.Text.Json;
using System.Threading.Channels;

namespace RabbitMQ_Opgave.Components;

public partial class Receiver
{
    public Receiver()
    {
        _messageList = new();
    }

    [Parameter]
    public string RoutingKey { get; set; }

    private List<SenderModel> _messageList;
    private string _exchange = "AdminAppTask";
    private string _queueName;
    private EventingBasicConsumer _consumer;
    private IModel _channel;


    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender)
        {
            // Create channel.
            var factory = new ConnectionFactory { HostName = "localhost" };
            var connection = factory.CreateConnection();
            _channel = connection.CreateModel();
            _channel.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Topic);

            // Set fields.
            _queueName = $"{_exchange}_{RoutingKey}";
            //_channel.QueueDeclare(_queueName);

            // Declare DLX 
            string dlxExchangeName = $"dlx_{_exchange}";
            _channel.ExchangeDeclare(dlxExchangeName, ExchangeType.Fanout);
            //string dlxQueueName = $"{dlxExchangeName}_queue";
            //_channel.QueueDeclare(dlxQueueName, true, false, false, null);

            // Create the queue, with dead lettering enabled
            _channel.QueueDeclare(
                queue: _queueName, 
                durable: true, 
                arguments: new Dictionary<string, object>()
                {
                    {"x-dead-letter-exchange", dlxExchangeName }
                }); //der er også exclusive or autoDelete, but they are just default values

            // Create consumer.
            _consumer = new EventingBasicConsumer(_channel);

            // Do the event.
            _consumer.Received += ReceiveInfo;

            // Bind to queue.
            _channel.QueueBind(queue: _queueName, exchange: _exchange, routingKey: RoutingKey);

            // Start consuming.
            _channel.BasicConsume(_queueName, false, _consumer);
        }

        base.OnAfterRender(firstRender);
    }


    public void ReceiveInfo(object? model, BasicDeliverEventArgs args)
    {
        SenderModel? messageObject;
        if (!IsMessageValid(args, out messageObject))
        {
            if (RoutingKey.Equals("tour.*"))
            {
                _channel.BasicNack(args.DeliveryTag, false, false);
            }
            return;
        }
        _channel.BasicAck(args.DeliveryTag, false);

        InvokeAsync(() =>
        {
            _messageList.Add(messageObject);
            StateHasChanged();
        });
    }

    public bool IsMessageValid(BasicDeliverEventArgs ea, out SenderModel? messageObject)
    {
        try
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            messageObject = JsonSerializer.Deserialize<SenderModel>(message);

            if (messageObject is null)
            {
                return false;
            }
            return messageObject.IsValid();
        }
        catch (Exception)
        {
            messageObject = null;
            return false;
        }
    }
}
