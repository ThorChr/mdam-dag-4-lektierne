﻿using RabbitMQ.Client;
using RabbitMQ_Opgave.Models;
using System.Text;
using System.Text.Json;

namespace RabbitMQ_Opgave.Components;

public partial class Send
{
    public Send()
    {
        _sender = new();
    }
    private SenderModel _sender;
    private string _exchange = "AdminAppTask";

    public void SendInfo()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };

        using var connection = factory.CreateConnection();
        using var channel = connection.CreateModel();

        channel.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Topic);

        //Coolest switch case
        var routingKey = _sender.Booked switch
        {
            true => "tour.booked",
            false => "tour.canceled"
        };

        var message = JsonSerializer.Serialize(_sender);

        var body = Encoding.UTF8.GetBytes(message);
        channel.BasicPublish(exchange: _exchange,
                             routingKey: routingKey,
                             basicProperties: null,
                             body: body);
        Console.WriteLine($" [x] Sent '{routingKey}':'{message}'");
    }
}
