﻿using RabbitMQ.Client;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMqLibrary.Services;
using System.Text.Json;

namespace BlazorServerAppYes.Components;

public partial class SenderComponent
{
    private FunMessageYesYesYeeeees _messageToYeet;
    private IMessageSender _messageSender;
    private string _exchange = "MikoMiku";

    public SenderComponent()
    {
        _messageToYeet = new();
        var channel = RabbitMqConnectionFactory.CreateChannel("localhost", _exchange, ExchangeType.Topic);
        _messageSender = new RabbitMqService(channel);
    }

    public void HandleValidSubmit()
    {
        string routingKey = _messageToYeet.Booked switch
        {
            true => "tour.booked",
            false => "tour.notBookedIGuess"
        };

        // Create a message and yeet it.
        MessageModel message = new()
        {
            Exchange = _exchange,
            Message = JsonSerializer.Serialize(_messageToYeet),
            RoutingKey = routingKey
        };

        _messageSender.SendMessage(message);
    }
}
