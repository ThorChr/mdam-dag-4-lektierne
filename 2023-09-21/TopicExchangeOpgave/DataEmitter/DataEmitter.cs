﻿using RabbitMQ.Client;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Services;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Models;
using System.Text.Json;

string exchange = "funfuntask";

var channel = RabbitMqConnectionFactory.CreateChannel("localhost", exchange, ExchangeType.Topic);
IMessageSender sender = new RabbitMqService(channel);

while(true)
{
    // Gather info from user.
    Console.WriteLine("Name > ");
    string name = Console.ReadLine() ?? "Chad G. Pete";

    Console.WriteLine("Email > ");
    string email = Console.ReadLine() ?? "chadgp@llm.com";

    Console.WriteLine("Tours > ");
    string tour = Console.ReadLine() ?? "idk, Taylor Swift world tour???";

    Console.WriteLine("Book or Cancel?");
    Console.WriteLine("1 - Book");
    Console.WriteLine("2 - Cancel");
    string option = Console.ReadLine() ?? "2";

    bool dudeIsBooking = option.Equals("1");

    // Setup message and routing.
    FunMessageYesYesYeeeees messageContent = new(name, email, dudeIsBooking);

    string routingKey = dudeIsBooking switch
    {
        true => "tour.booked",
        false => "tour.notBookedIGuess"
    };

    // Create a message and yeet it.
    MessageModel message = new()
    {
        Exchange = exchange,
        Message = JsonSerializer.Serialize(messageContent),
        RoutingKey = routingKey
    };

    sender.SendMessage(message);
}



//// Create the factory.
//var factory = new ConnectionFactory { HostName = "localhost" };

//// Establish a connection and join a channel.
//using var connection = factory.CreateConnection();
//using var channel = connection.CreateModel();

//// Declare what exchange to use.
//channel.ExchangeDeclare(exchange: "topic_logs", type: ExchangeType.Topic);

//// Setup data.
//while(true)
//{
//    Console.WriteLine("Provide a routing key.");
//    Console.Write("> ");
//    string routingKey = Console.ReadLine() ?? "hutao";

//    Console.WriteLine("Write a message.");
//    Console.Write("> ");
//    string message = Console.ReadLine() ?? "cutie";

//    var body = Encoding.UTF8.GetBytes(message);
//    // Yeet data.
//    channel.BasicPublish(exchange: "topic_logs",
//                         routingKey: routingKey,
//                         basicProperties: null,
//                         body: body);
//    Console.WriteLine($" [x] Sent '{routingKey}':'{message}'");
//}