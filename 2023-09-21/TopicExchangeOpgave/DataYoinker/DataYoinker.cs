﻿using System.Text;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

string exchange = "funfuntask";

var channel = RabbitMqConnectionFactory.CreateChannel("localhost", exchange, ExchangeType.Topic);
IMessageReceiver receiver = new RabbitMqService(channel);

string queueName = receiver.GetQueueName();

foreach(var bindingKey in args)
{
    receiver.BindQueue(queueName, exchange, bindingKey);
}

Console.WriteLine(" [*] Waiting for messages. To exit press CTRL+C");

var consumer = new EventingBasicConsumer(channel);
consumer.Received += (model, ea) =>
{
    var body = ea.Body.ToArray();
    var message = Encoding.UTF8.GetString(body);
    var routingKey = ea.RoutingKey;
    Console.WriteLine($" [x] Received '{routingKey}':'{message}'");
};

channel.BasicConsume(queue: queueName,
                     autoAck: true,
                     consumer: consumer);

Console.WriteLine(" Press [enter] to exit.");
Console.ReadLine();


//ConnectionFactory factory = new() { HostName = "localhost" };
//using var connection = factory.CreateConnection();
//using var channel = connection.CreateModel();

//// topic_logs matches up with the name from DataEmitter.cs
//channel.ExchangeDeclare("topic_logs", ExchangeType.Topic);

//string queueName = channel.QueueDeclare().QueueName;

//// Find out what binding key to use, this should be provided by the command line arguments.
//if (args.Length < 1)
//{
//    Console.Error.WriteLine("Usage: {0} [binding_key...]", Environment.GetCommandLineArgs()[0]);
//    Console.WriteLine(" Press [enter] to exit.");
//    Console.ReadLine();
//    Environment.ExitCode = 1;
//    return;
//}

//foreach (var bindingKey in args)
//{
//    channel.QueueBind(queue: queueName, exchange: "topic_logs", routingKey: bindingKey);
//}

//Console.WriteLine(" [*] Waiting for messages. To exit press CTRL+C");

//var consumer = new EventingBasicConsumer(channel);
//consumer.Received += (model, ea) =>
//{
//    var body = ea.Body.ToArray();
//    var message = Encoding.UTF8.GetString(body);
//    var routingKey = ea.RoutingKey;
//    Console.WriteLine($" [x] Received '{routingKey}':'{message}'");
//};

//channel.BasicConsume(queue: queueName,
//                     autoAck: true,
//                     consumer: consumer);

//Console.WriteLine(" Press [enter] to exit.");
//Console.ReadLine();
