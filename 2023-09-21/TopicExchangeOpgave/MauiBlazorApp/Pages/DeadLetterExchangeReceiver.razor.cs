﻿using Microsoft.AspNetCore.Components;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMqLibrary.Services;
using System.Text;
using RabbitMqLibrary;

namespace MauiBlazorApp.Pages;

public partial class DeadLetterExchangeReceiver
{
    private IMessageReceiver _messageReceiver;
    private string _queueName = String.Empty;
    private EventingBasicConsumer _consumer;

    private List<string> _messagesReceived = new List<string>();

    protected override void OnInitialized()
    {
        var channel = RabbitMqConnectionFactory.CreateChannel("localhost", LibraryConstants.DeadLetterExchangeName, ExchangeType.Fanout);
        _messageReceiver = new RabbitMqService(channel);
        _queueName = $"{LibraryConstants.DeadLetterExchangeName}_queue";

        // Declare queue.
        QueueDeclarationModel declaration = new()
        {
            QueueName = _queueName,
            Durable = true
        };

        _messageReceiver.DeclareQueue(declaration);

        // Bind queue.
        QueueModel queueModel = new()
        {
            QueueName = _queueName,
            Exchange = LibraryConstants.DeadLetterExchangeName,
            RoutingKey = ""
        };

        _messageReceiver.BindQueue(queueModel);

        // Start consuming.
        _consumer = new EventingBasicConsumer(channel);
        _consumer.Received += HandleMessageReceived;
        channel.BasicConsume(_queueName, false, _consumer);

        base.OnInitialized();
    }

    public void HandleMessageReceived(object? model, BasicDeliverEventArgs args)
    {
        string jsonPayload = Encoding.UTF8.GetString(args.Body.ToArray());

        // Switch to the UI thread to update UI components
        InvokeAsync(() =>
        {
            _messagesReceived.Add(jsonPayload);
            StateHasChanged();
        });

        _messageReceiver.AcknowledgeMessage(args);
    }
}