﻿using Microsoft.AspNetCore.Components;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMqLibrary;
using RabbitMqLibrary.Factories;
using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMqLibrary.Services;
using System.Text;
using System.Text.Json;

namespace MauiBlazorApp.Pages;

public partial class ReceiverPage
{
    [Parameter]
    public string BindingKey { get; set; }

    private IMessageReceiver _messageReceiver;
    private string _queueName = String.Empty;
    private EventingBasicConsumer _consumer;

    private List<string> _messagesReceived = new List<string>();

    protected override void OnInitialized()
    {
        _queueName = $"{LibraryConstants.ExchangeName}_queue_{BindingKey}";

        var channel = RabbitMqConnectionFactory.CreateChannel("localhost", LibraryConstants.ExchangeName, ExchangeType.Topic);
        _messageReceiver = new RabbitMqService(channel);

        channel.ExchangeDeclare(LibraryConstants.DeadLetterExchangeName, ExchangeType.Fanout);

        // Create the queue, with dead lettering enabled.
        QueueDeclarationModel declarationModel = new()
        {
            QueueName = _queueName,
            Durable = true,
            Arguments = new Dictionary<string, object>()
            {
                { "x-dead-letter-exchange", LibraryConstants.DeadLetterExchangeName }
            }
        };
        _messageReceiver.DeclareQueue(declarationModel);

        // Create consumer and subscribe to the event.
        _consumer = new EventingBasicConsumer(channel);
        _consumer.Received += HandleMessageReceived;

        // Bind to our queues.
        _messageReceiver.BindQueue(_queueName, LibraryConstants.ExchangeName, BindingKey);

        // Start consuming.
        var consumerTag = channel.BasicConsume(_queueName, false, _consumer);
        Console.WriteLine($"Consumer with tag {consumerTag} is consuming from {_queueName}");

        base.OnInitialized();
    }

    public void HandleMessageReceived(object? model, BasicDeliverEventArgs args)
    {
        FunMessageYesYesYeeeees? messageObject;

        if (!IsMessageValid(args, out messageObject))
        {
            // Yeet over to DLX.
            if(BindingKey.Equals("tour.*"))
            {
                _messageReceiver.RejectMessage(args);
            }

            return;
        }

        _messageReceiver.AcknowledgeMessage(args);

        string payloadContent = $"User '{messageObject.Name}' with email '{messageObject.Email}' has done something. Booking status: {messageObject.Booked}";
        
        // Switch to the UI thread to update UI components
        InvokeAsync(() =>
        {
            _messagesReceived.Add(payloadContent);
            StateHasChanged();
        });
    }

    private bool IsMessageValid(BasicDeliverEventArgs args, out FunMessageYesYesYeeeees? messageObject)
    {
        try
        {
            string jsonPayload = Encoding.UTF8.GetString(args.Body.ToArray());
            messageObject = JsonSerializer.Deserialize<FunMessageYesYesYeeeees>(jsonPayload);

            if (messageObject is null)
            {
                return false;
            }

            return messageObject.IsValid();
        }
        catch(Exception)
        {
            messageObject = null;
            return false;
        }
    }
}
