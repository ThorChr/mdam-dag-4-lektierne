﻿using RabbitMQ.Client.Events;
using RabbitMqLibrary.Models;

namespace RabbitMqLibrary.Interfaces;

public interface IMessageReceiver
{
    void BindQueue(string queueName, string exchange, string routingKey);
    void BindQueue(QueueModel queueModel);
    void DeclareQueue(string queueName);
    void DeclareQueue(QueueDeclarationModel queueDeclaration);
    [Obsolete]
    string GetQueueName();
    void DeclareDeadLetterExchange(string exchangeName);
    void DeclareDeadLetterQueue(string queueName);
    void AcknowledgeMessage(BasicDeliverEventArgs args);
    void RejectMessage(BasicDeliverEventArgs args);
}
