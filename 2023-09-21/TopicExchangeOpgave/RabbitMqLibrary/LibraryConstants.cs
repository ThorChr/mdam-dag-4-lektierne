﻿namespace RabbitMqLibrary;

public static class LibraryConstants
{
    public const string ExchangeName = "MikoMiku";
    public const string DeadLetterExchangeName = "dlx_MikoMiku";
}
