﻿using System.ComponentModel.DataAnnotations;

namespace RabbitMqLibrary.Models;

public class FunMessageYesYesYeeeees
{
    [Required]
    public string Name { get; set; }

    [Required]
    public string Email { get; set; }

    public bool Booked { get; set; }

    public FunMessageYesYesYeeeees(string name, string email, bool booked)
    {
        Name = name;
        Email = email;
        Booked = booked;
    }

    public FunMessageYesYesYeeeees() 
    {
        Name = String.Empty;
        Email = String.Empty;
        Booked = false;
    }

    public bool IsValid()
    {
        return !String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(Email);
    }
}