﻿namespace RabbitMqLibrary.Models;

public class QueueDeclarationModel
{
    public string QueueName { get; set; } = String.Empty;
    public bool Durable { get; set; } = false;
    public bool Exclusive { get; set; } = false;
    public bool AutoDelete { get; set; } = false;
    public IDictionary<string, object>? Arguments { get; set; } = null;

    public QueueDeclarationModel()
    {
        
    }

    public QueueDeclarationModel(string queueName)
    {
        QueueName = queueName;
    }

    public QueueDeclarationModel(string queueName, bool durable, bool exclusive, bool autoDelete, IDictionary<string, object>? arguments)
    {
        QueueName = queueName;
        Durable = durable;
        Exclusive = exclusive;
        AutoDelete = autoDelete;
        Arguments = arguments;
    }
}
