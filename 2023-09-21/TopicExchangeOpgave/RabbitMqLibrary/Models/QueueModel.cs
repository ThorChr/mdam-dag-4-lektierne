﻿namespace RabbitMqLibrary.Models;

public class QueueModel
{
    public string QueueName { get; set; } = String.Empty;
    public string Exchange { get; set; } = String.Empty;
    public string RoutingKey { get; set; } = String.Empty;
}
