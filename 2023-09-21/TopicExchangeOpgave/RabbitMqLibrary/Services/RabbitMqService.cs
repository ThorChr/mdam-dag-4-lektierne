﻿using RabbitMqLibrary.Interfaces;
using RabbitMqLibrary.Models;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ.Client.Events;

namespace RabbitMqLibrary.Services;

public class RabbitMqService : IMessageSender, IMessageReceiver
{
    private readonly IModel _channel;

    public RabbitMqService(IModel channel)
    {
        _channel = channel;
    }

    public void SendMessage(string routingKey, string message, string exchange)
    {
        byte[] body = Encoding.UTF8.GetBytes(message);

        // Ensure message persistancy.
        var basicProperties = _channel.CreateBasicProperties();
        basicProperties.Persistent = true;

        _channel.BasicPublish(exchange: exchange, routingKey: routingKey, basicProperties: basicProperties, body: body);
    }

    public void SendMessage(MessageModel messageModel) 
        => SendMessage(messageModel.RoutingKey, messageModel.Message, messageModel.Exchange);

    public void BindQueue(string queueName, string exchange, string routingKey)
    {
        _channel.QueueBind(queue: queueName, exchange: exchange, routingKey: routingKey);
    }

    public void BindQueue(QueueModel queueModel) 
        => BindQueue(queueModel.QueueName, queueModel.Exchange, queueModel.RoutingKey);

    public void DeclareQueue(string queueName)
    {
        _channel.QueueDeclare(queueName);
    }

    public void DeclareQueue(QueueDeclarationModel queueDeclaration)
    {
        _channel.QueueDeclare(
            queueDeclaration.QueueName, 
            queueDeclaration.Durable,
            queueDeclaration.Exclusive, 
            queueDeclaration.AutoDelete, 
            queueDeclaration.Arguments);
    }

    public string GetQueueName()
    {
        return _channel.QueueDeclare().QueueName;
    }

    public void DeclareDeadLetterExchange(string exchangeName)
    {
        _channel.ExchangeDeclare(exchangeName, ExchangeType.Fanout);
    }

    public void DeclareDeadLetterQueue(string queueName)
    {
        _channel.QueueDeclare(queueName, true, false, false, null);
    }

    public void AcknowledgeMessage(BasicDeliverEventArgs args)
    {
        _channel.BasicAck(args.DeliveryTag, false);
    }
    public void RejectMessage(BasicDeliverEventArgs args)
    {
        _channel.BasicNack(args.DeliveryTag, false, false);
    }
}
